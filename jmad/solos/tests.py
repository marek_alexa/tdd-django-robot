from django.test import TestCase
from .views import index


class IndexView(TestCase):

    def test_localhost(self):
        home_page = self.client.get('/')

    def test_root_url_uses_index_view(self):
        """
        Test that the root of the site resolves to the correct
        view function.
        """
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')
